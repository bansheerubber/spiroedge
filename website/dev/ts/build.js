// builds the website and puts it into /website/dist

const { exec } = require('child_process')
const process = require('process')

// call webpack -p

// delete all files in /website/dist
const fs = require('fs')

const deleteIgnore = ["d"] // subdirectories we don't want to delete
deleteFiles("../../dist")

function deleteFiles(directoryName) {
	fs.readdir(directoryName, (error, files) => {
		for(var file of files) {
			if(isDirectory(file)) { // recursively delete directories
				if(deleteIgnore.indexOf(file) == -1) {
					deleteFiles(directoryName + "/" + file)
				}
			}
			else // delete the file
				fs.unlink(directoryName + "/" + file, () => {})
		}

		// remove the directory once everything in it is deleted
		if(directoryName != "../../dist") {
			setTimeout(() => {
				fs.rmdir(directoryName, () => {})
			}, 100)
		}
	})
}


// now copy over everything except the shit in the ignore filters
const extensionIgnore = ["js", "ts", "jsx", "tsx", "html"]
const copyIgnore = ["ts"]

copyFiles("..", "../../dist")

function copyFiles(directoryName, destination) {
	fs.readdir(directoryName, (error, files) => {
		for(var file of files) {
			// recursively copy directories
			if(isDirectory(file)) {
				if(copyIgnore.indexOf(file) == -1) {
					copyFiles(directoryName + "/" + file, destination + "/" + file)
				}
			}
			else if(extensionIgnore.indexOf(getFileExtension(file)) == -1) // copy the file over if it doesn't have one of the file extensions
			{
				// if the directory does not exist, then create it
				if(!fs.existsSync(destination))
					 fs.mkdirSync(destination)
					 
				fs.copyFile(directoryName + "/" + file, destination + "/" + file, (error) => {
				 	if(error) 
				 		throw error
				})
			}
		}
	})
}


// now call webpack -p, which transpiles typescript and everything else
exec('webpack -p', (error, stdout, stderror) => {
	console.log(stdout);
})


// tests if file string is a directory or not
function isDirectory(file) {
	return file.indexOf(".") == -1
}

// returns the extension of the file
function getFileExtension(file) {
    return file.match(/[^\\]*\.(\w+)$/)[1]
}