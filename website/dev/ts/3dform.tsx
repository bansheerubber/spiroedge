import * as React from 'react'
import * as THREE from 'three'
import { Pane3D } from './3d';

interface Form3DProperty {
	source: Pane3D
}

class Form3DProperties2 {
	count: number;
}

export class Form3D extends React.Component<Form3DProperty, Form3DProperties2> {
	x: number = 0;
	y: number = 0;
	z: number = 0;
	source: Pane3D;
	
	constructor(props: Form3DProperty) {
		super(props)
	}

	setPositionX(event: React.ChangeEvent<HTMLInputElement>) {
		this.x = parseInt(event.target.value)
		this.updateThreeJS()
	}

	setPositionY(event: React.ChangeEvent<HTMLInputElement>) {
		this.y = parseInt(event.target.value)
		this.updateThreeJS()
	}

	setPositionZ(event: React.ChangeEvent<HTMLInputElement>) {
		console.log(this)
		this.z = parseInt(event.target.value)
		this.updateThreeJS()
	}

	updateThreeJS() {
		this.props.source.setCameraPosition(new THREE.Vector3(this.x, this.y, this.z))
		console.log("updated camera position")
	}

	render() {
		return <div>
			<input type="text" placeholder="x" onChange={this.setPositionX.bind(this)} />
			<input type="text" placeholder="y" onChange={this.setPositionY.bind(this)} />
			<input type="text" placeholder="z" onChange={this.setPositionZ.bind(this)} />
		</div>
	}
}