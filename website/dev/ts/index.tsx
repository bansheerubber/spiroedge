import * as ReactDOM from 'react-dom'
import * as React from 'react'
import { Pane3D } from './3d'
import { Form3D } from './3dform'

let pane3d = <Pane3D></Pane3D>
ReactDOM.render(pane3d, document.getElementById("pane3D"))