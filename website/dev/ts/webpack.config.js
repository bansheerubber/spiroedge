const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './index.tsx',
	// devtool: 'inline-source-map',
	plugins: [
		new CleanWebpackPlugin(['../../dist']),
		new HtmlWebpackPlugin({
			template: '../index.html'
		}),
	],
	module: {
		rules: [{
			test: /\.tsx?$/,
			use: 'ts-loader',
			exclude: /node_modules/
		}]
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js']
	},
	output: {
		path: path.resolve(__dirname, '../../dist'),
		filename: 'js/dist.js'
	}
};