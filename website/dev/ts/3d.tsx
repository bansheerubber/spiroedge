import * as THREE from 'three' // made three not all caps because holy shit that's a pain in the dick to type out
import * as React from 'react'
import {Form3D} from './3dform'
import { throws } from 'assert';

interface Pane3DProperties {

}

class MouseHandler {
    startMouseX: number
    startMouseY: number
    mouseX: number
    mouseY: number
    
    startMove(x: number, y: number) {
        this.startMouseX = x
        this.startMouseY = y
    }

    // calculates the change from the start position to the current position
    getDelta() {
        return {
            x: this.startMouseX - this.mouseX,
            y: this.startMouseY - this.mouseY
        }
    }

    constructor() {

    }
}

// code for handling the 3D rendering of the spiroedge
export class Pane3D extends React.Component<Pane3DProperties, {}> {
    camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(45, 1920 / 1080, 1, 1000)
    scene: THREE.Scene
    ambientLight: THREE.AmbientLight
    loadingManager: THREE.LoadingManager
    renderer: THREE.WebGLRenderer
    tick: number = 0
    object: THREE.Group
    mouseHandler: MouseHandler;
    
    constructor(props: Pane3DProperties) {
        super(props);

        // set up the scene
        this.scene = new THREE.Scene()
        this.ambientLight = new THREE.AmbientLight(0xFFFFFF, 0.4)

        this.setCameraPosition(new THREE.Vector3(0, 0, 5))

        this.camera.add(new THREE.PointLight(0xFFFFFF, 0.8))
        this.scene.add(this.camera)
        this.scene.add(this.ambientLight)

        this.loadingManager = new THREE.LoadingManager()
        this.loadingManager.onLoad = this.onLoad.bind(this)

        this.renderer = new THREE.WebGLRenderer()
        this.renderer.setPixelRatio(1)
        this.renderer.setSize(1920, 1080)

        this.loadObject("./d/test.obj").then((object: THREE.Group) => {
            this.object = object
            this.renderGL();
        })
    }

    render() {
        document.getElementById("pane3D").appendChild(this.renderer.domElement)
        return ""
    }

    renderGL() {
        this.renderer.render(this.scene, this.camera)
        requestAnimationFrame(this.renderGL.bind(this))

        this.object.setRotationFromEuler(new THREE.Euler(0, 0, Math.sin(this.tick / 200)));

        this.tick++
    }

    setCameraLookAt(vector: THREE.Vector3) {
        this.camera.lookAt(vector)
        this.camera.updateProjectionMatrix();
    }
    
    setCameraPosition(position: THREE.Vector3) {
        this.camera.position.x = position.x;
        this.camera.position.y = position.y;
        this.camera.position.z = position.z;
        this.camera.updateProjectionMatrix();
    }

    // called when we're finished loading an asset using this.loadingManager
    onLoad() {
        
    }

    loadObject(file: string) {
        let objectLoader = new THREE.OBJLoader(this.loadingManager)

        return new Promise<THREE.Group>((resolve, reject) => {
            objectLoader.load(file, (object: THREE.Group) => {
                this.scene.add(object)
                resolve(object)
            })
        })
    }
}